use std::sync::Arc;

use anyhow::bail;
use tokio::sync::{mpsc, Mutex};
use tracing::info;

use crate::mcts;
use crate::{game, mcts::MctsSolver};
use crate::{Solver, State};

pub fn init(size: usize, p1: PlayerType, p2: PlayerType) {
    show_image::run_context(move || {
        _init(size, p1, p2).unwrap();
    });
}

#[derive(Debug)]
pub enum PlayerType {
    Mcts,
    User,
}

impl PlayerType {
    fn agent<S: State>(self) -> Option<Box<dyn Solver<S>>> {
        match self {
            PlayerType::Mcts => Some(Box::new(MctsSolver)),
            PlayerType::User => None,
        }
    }
}

impl std::str::FromStr for PlayerType {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "mcts" => Ok(PlayerType::Mcts),
            "user" => Ok(PlayerType::User),
            _ => bail!("Failed to parse {} into PlayerType", s),
        }
    }
}

// UI code
fn _init(board_size: usize, p1: PlayerType, p2: PlayerType) -> anyhow::Result<()> {
    let (tx, mut rx) = mpsc::unbounded_channel();
    let mut board = game::Board::new(board_size);
    let history = Arc::new(Mutex::new(vec![board.clone()]));

    let _history = history.clone();
    std::thread::spawn(move || {
        let runtime = tokio::runtime::Builder::new_current_thread()
            .build()
            .unwrap();
        runtime.block_on(async {
            let history = _history.clone();

            let mut a1 = p1.agent();
            let mut a2 = p2.agent();

            while !board.is_ended() {
                let action = if board.age() % 2 == 0 {
                    if let Some(agent) = &mut a1 {
                        agent.solve(&board)
                    } else {
                        rx.recv().await.unwrap()
                    }
                } else {
                    if let Some(agent) = &mut a2 {
                        agent.solve(&board)
                    } else {
                        rx.recv().await.unwrap()
                    }
                };
                if board.try_place(action).is_ok() {
                    info!("pushing");
                    history.lock().await.push(board.clone());
                    info!("pushed");
                }
            }
        });
    });

    let window = show_image::create_window("Game view", Default::default())?;

    let runtime = tokio::runtime::Builder::new_current_thread()
        .build()
        .unwrap();
    runtime.block_on(async { display(&window, history, tx).await })?;

    Ok(())
}

fn save(history: &Vec<game::Board>) -> anyhow::Result<()> {
    let timestamp = chrono::Local::now().format("%Y-%m-%d-%H:%M:%S").to_string();
    let mut outdir = std::path::Path::new("outputs").to_path_buf();
    outdir.push(&timestamp);
    std::fs::create_dir_all(&outdir)?;

    for (i, board) in history.iter().enumerate() {
        outdir.push(format!("{}.png", i));
        let img = board.image();
        img.save(&outdir)?;
        outdir.pop();
    }

    Ok(())
}

async fn display(
    window: &show_image::WindowProxy,
    history: Arc<Mutex<Vec<game::Board>>>,
    tx: mpsc::UnboundedSender<(usize, usize)>,
) -> anyhow::Result<()> {
    use show_image::event::{
        ElementState, KeyboardInput, VirtualKeyCode, WindowEvent, WindowKeyboardInputEvent,
    };
    use VirtualKeyCode::*;

    let mut cur = 0;

    'outer: loop {
        let history_lock = history.lock().await;
        let ref board = history_lock[cur];
        board.print();
        let img = board.image();
        drop(history_lock);
        window.set_image("img", img)?;

        let mut input_mode = false;
        let mut keybuf = vec![];
        let push_key = |key, input_mode, keybuf: &mut Vec<usize>| {
            if input_mode {
                keybuf.push(key)
            }
        };

        for event in window.event_channel()? {
            if let WindowEvent::KeyboardInput(WindowKeyboardInputEvent {
                input:
                    KeyboardInput {
                        key_code: Some(key_code),
                        state: ElementState::Pressed,
                        ..
                    },
                ..
            }) = event
            {
                match key_code {
                    Key0 => push_key(0, input_mode, &mut keybuf),
                    Key1 => push_key(1, input_mode, &mut keybuf),
                    Key2 => push_key(2, input_mode, &mut keybuf),
                    Key3 => push_key(3, input_mode, &mut keybuf),
                    Key4 => push_key(4, input_mode, &mut keybuf),
                    Key5 => push_key(5, input_mode, &mut keybuf),
                    Key6 => push_key(6, input_mode, &mut keybuf),
                    Key7 => push_key(7, input_mode, &mut keybuf),
                    Key8 => push_key(8, input_mode, &mut keybuf),
                    Key9 => push_key(9, input_mode, &mut keybuf),
                    A => push_key(10, input_mode, &mut keybuf),
                    B => push_key(11, input_mode, &mut keybuf),
                    C => push_key(12, input_mode, &mut keybuf),
                    D => push_key(13, input_mode, &mut keybuf),
                    E => push_key(14, input_mode, &mut keybuf),
                    F => push_key(15, input_mode, &mut keybuf),
                    G => push_key(16, input_mode, &mut keybuf),
                    H => push_key(17, input_mode, &mut keybuf),
                    I => push_key(18, input_mode, &mut keybuf),
                    M => {
                        input_mode = !input_mode;
                        info!(
                            "Input mode {}",
                            if input_mode { "enabled" } else { "disabled" }
                        );
                    }
                    N => {
                        let history_lock = history.lock().await;
                        cur = (cur + 1).min(history_lock.len() - 1);
                        drop(history_lock);
                        break;
                    }
                    P => {
                        cur = cur.saturating_sub(1);
                        break;
                    }
                    Q | Escape => {
                        info!("exit pressed");
                        break 'outer;
                    }
                    S => {
                        info!("Saving to disk");
                        save(history.lock().await.as_ref())?;
                    }
                    Z => {
                        cur = 0;
                        break;
                    }
                    X => {
                        let history_lock = history.lock().await;
                        cur = history_lock.len() - 1;
                        drop(history_lock);
                        break;
                    }
                    _ => {}
                }

                if keybuf.len() == 2 {
                    let (i, j) = (keybuf[0], keybuf[1]);
                    tx.send((i, j))?;
                }
            }
        }
    }

    Ok(())
}
