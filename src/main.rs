use std::fmt::Debug;

use anyhow::bail;
use rand::prelude::*;
use std::sync::Arc;
use structopt::StructOpt;
use tokio::sync::{mpsc, Mutex};
use tracing::info;

mod analyze;
mod game;
mod localui;
mod math;
mod mcts;

fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();
    match Args::from_args() {
        Args::Play { size, p1, p2 } => {
            localui::init(size, p1, p2);
        }
    }

    Ok(())
}

#[derive(Debug, StructOpt)]
enum Args {
    Play {
        size: usize,
        p1: localui::PlayerType,
        p2: localui::PlayerType,
    },
}

// Random solver
struct RandSolver;

impl<S> Solver<S> for RandSolver
where
    S: State,
{
    fn solve(&mut self, state: &S) -> S::A {
        state.get_legal_actions().choose(&mut thread_rng()).unwrap()
    }
}

// Trait defs
trait Solver<S>
where
    S: State,
{
    fn solve(&mut self, state: &S) -> S::A;
}

trait State: Debug + Clone {
    type A: Action + Debug + Clone;
    type P: Player + Debug + Clone + Eq;

    fn get_legal_actions(&self) -> Box<dyn Iterator<Item = Self::A> + '_>;
    fn winner(&self) -> Option<Self::P>;
    fn is_ended(&self) -> bool;
    fn apply(&mut self, action: Self::A);
    fn next(&self) -> Self::P;
}

trait Action {}

trait Player {}

// Impl state and action for gomoku board

impl State for game::Board {
    type A = (usize, usize);
    type P = game::Stone;

    fn get_legal_actions(&self) -> Box<dyn Iterator<Item = (usize, usize)> + '_> {
        Box::new(self.get_empty_places())
    }

    fn is_ended(&self) -> bool {
        self.is_ended()
    }

    fn apply(&mut self, action: (usize, usize)) {
        self.place(action);
    }

    fn next(&self) -> Self::P {
        self.next()
    }

    fn winner(&self) -> Option<Self::P> {
        self.winner()
    }
}

impl Action for (usize, usize) {}

impl Player for game::Stone {}
